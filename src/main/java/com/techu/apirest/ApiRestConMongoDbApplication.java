package com.techu.apirest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiRestConMongoDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiRestConMongoDbApplication.class, args);
	}

}
